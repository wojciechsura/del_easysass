# EasySass README

It's about time someone created a standalone extension to compile SASS/SCSS files in Visual Studio Code. Don't you think?

## Features

Automatically compiles SASS/SCSS files to .css and .min.css upon saving. You may also quickly compile all SCSS/SASS files in your project.

![Demo](demo.gif)

## Extension Settings

This extension contributes the following settings:

* `easysass.generateExpanded`: generate expanded, human-readable CSS file (file.scss -> file.css)
* `easysass.generateMinified`: generate minified CSS file (file.scss -> file.min.css)

## Release Notes

### 0.0.1

Initial release of EasySass

**Enjoy!**